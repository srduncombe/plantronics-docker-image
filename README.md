# Docker Image for GSA2 Testing

This docker container is used for testing the GSA2 web application via BitBucket's pipeline. It is [hosted here](https://hub.docker.com/r/srduncombe/plantronics_gsa2_pipeline/) on the Docker Hub.

It is based off a [Python2.7 / Postgres9.4](https://hub.docker.com/r/valtechcanada/python-postgres/) image but also includes PhantomJS and the Heroku CLI.

To build the container locally run `docker build .` from the root directory.
